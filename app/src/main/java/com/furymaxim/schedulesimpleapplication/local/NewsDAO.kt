package com.furymaxim.schedulesimpleapplication.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.furymaxim.schedulesimpleapplication.models.News

@Dao
interface NewsDAO {


    @get:Query("SELECT * FROM news")
    val allNews: List<News>

    @Query("SELECT * FROM news WHERE id=:newsId")
    fun getNewsById(newsId:Int):News

    @Insert
    fun insertNews(news: News)

    @Delete
    fun deleteNews(news: News)

    @Query("DELETE FROM news")
    fun deleteAllNews()

    @Query("SELECT COUNT(*) FROM news")
    fun getCount():Int


    @Query("SELECT * FROM news WHERE id>:startId and id<:endId")
    fun getNewsByIdRange(startId:Int, endId:Int):List<News>

}