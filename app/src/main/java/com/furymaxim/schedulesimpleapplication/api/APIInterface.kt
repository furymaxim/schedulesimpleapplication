package com.furymaxim.schedulesimpleapplication.api

import com.furymaxim.schedulesimpleapplication.models.BasicModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface APIInterface {



    /*просто достаточно будет номера страницы*/

    @GET("top_rated")
    fun getTopRatedMovies(@Query("api_key") apiKey: String?, @Query("language") language: String?, @Query("page") pageIndex: Int): Call<BasicModel>?

}