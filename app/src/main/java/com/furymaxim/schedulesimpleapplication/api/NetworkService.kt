package com.furymaxim.schedulesimpleapplication.api

import com.furymaxim.schedulesimpleapplication.utils.Consts
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkService {
    private var mRetrofit: Retrofit? = null

    init{
        mRetrofit = Retrofit.Builder()
            .baseUrl(Consts.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getJSONApi():APIInterface{
        return mRetrofit!!.create(APIInterface::class.java)
    }

    companion object{
        private var mInstance: NetworkService? = null

        fun getInstance():NetworkService{

            if (mInstance == null) mInstance = NetworkService()

            return mInstance!!
        }
    }
}