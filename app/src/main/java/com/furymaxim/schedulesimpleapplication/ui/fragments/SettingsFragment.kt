package com.furymaxim.schedulesimpleapplication.ui.fragments


import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.furymaxim.schedulesimpleapplication.R
import com.furymaxim.schedulesimpleapplication.helpers.InternetConnectionChecker
import com.furymaxim.schedulesimpleapplication.utils.Consts
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : Fragment() {

    private var sharedPreferences: SharedPreferences? =null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = context!!.getSharedPreferences(Consts.PREF, Context.MODE_PRIVATE)

        if(sharedPreferences!!.contains("oldGroup")){
            groupEdit.setText(sharedPreferences!!.getString("oldGroup",""))
        }else{
            activity!!.bottomNavigation.menu.getItem(1).icon = resources.getDrawable(R.drawable.a_ic_timetable_grey)
        }

        if(!sharedPreferences!!.contains("notifPos")){
            noNotification.isChecked = true
        }else{
           // val notifPos = sharedPreferences!!.getInt("notifPos",0)

            when(sharedPreferences!!.getInt("notifPos",0)){
                0 -> noNotification.isChecked = true
                1 -> until20.isChecked = true
                2 -> until10.isChecked = true
                3 -> until5.isChecked = true
            }

        }

        groupEdit.isFocusable =false

        groupEdit.setOnClickListener {
            showAlertDialog(view)
        }


        noNotification.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                until20.isChecked = false
                until10.isChecked = false
                until5.isChecked = false
                sharedPreferences!!.edit().putInt("notifPos",0).apply()
            }
        }

        until20.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                noNotification.isChecked = false
                until5.isChecked = false
                until10.isChecked = false
                sharedPreferences!!.edit().putInt("notifPos",1).apply()
            }
        }

        until10.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                noNotification.isChecked = false
                until20.isChecked = false
                until5.isChecked = false
                sharedPreferences!!.edit().putInt("notifPos",2).apply()
            }
        }

        until5.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                noNotification.isChecked = false
                until20.isChecked = false
                until10.isChecked = false
                sharedPreferences!!.edit().putInt("notifPos",3).apply()
            }
        }

    }

    companion object {

        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }


    private fun showAlertDialog(view:View){

        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(context!!)
        val mView: View = layoutInflater.inflate(R.layout.dialog_group, null)
        val editText = mView.findViewById<EditText>(R.id.user_group)
        var isChangeSuccess = false
        alertDialog.setView(mView)

        alertDialog.setPositiveButton("Далее", object: DialogInterface.OnClickListener{
            var oldGroup:String? = null

            override fun onClick(dialog: DialogInterface?, which: Int) {

                val inputGroup = editText.text.toString()

                if(sharedPreferences!!.contains("oldGroup")){
                    oldGroup = sharedPreferences!!.getString("oldGroup","")
                }

                if(editText.text.length > 2){
                    if(oldGroup != null){
                        if(inputGroup.trim() != (oldGroup)){
                            if(isGroupValid(inputGroup.toInt())) {
                                sharedPreferences!!.edit().putString("oldGroup", inputGroup).apply()
                                isChangeSuccess = true
                                dialog!!.dismiss()

                            }else{
                                Toast.makeText(context,"Проверь интернет-соединение или группа не найдена",Toast.LENGTH_SHORT).show()
                                showAlertDialog(view)
                            }
                        }else{
                            // номер группы совпал со старой введенной
                            Toast.makeText(context,"Номер введенной группы совпал с текущей",Toast.LENGTH_SHORT).show()
                            showAlertDialog(view)
                        }
                    }else{
                        // в первый раз зашли
                        if(isGroupValid(inputGroup.toInt())){

                            sharedPreferences!!.edit().putString("oldGroup",inputGroup).apply()
                            isChangeSuccess = true
                            activity!!.bottomNavigation.menu.getItem(2).isVisible = true
                            activity!!.bottomNavigation.menu.getItem(2).isEnabled = true
                            activity!!.bottomNavigation.menu.getItem(2).icon = resources.getDrawable(R.drawable.a_ic_settings_grey)

                            dialog!!.dismiss()

                            activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragment, ScheduleFragment.newInstance())
                                .addToBackStack(null)
                                .commit()

                            //snackbar show
                        }else{
                           // isChangeSuccess = false
                            Toast.makeText(context,"Проверь интернет-соединение или группа не найдена",Toast.LENGTH_SHORT).show()
                            showAlertDialog(view)
                        }
                    }
                }else{
                   //длина меньше 3 символов
                    Toast.makeText(context,"Длина должна быть больше 3 символов",Toast.LENGTH_SHORT).show()
                    showAlertDialog(view)
                }
            }
        })


        alertDialog.setNegativeButton("Назад", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }
        })

        alertDialog.setOnDismissListener {
            if(isChangeSuccess){
                groupEdit.setText(sharedPreferences!!.getString("oldGroup",""))
                showSnackBar(view)
            }
        }



        val dialog = alertDialog.create()
        dialog.show()
    }



    private fun isGroupValid(group:Int):Boolean{


        return true


    }

    private fun showSnackBar(view: View){
        val snackBar = Snackbar.make(view,"Изменения успешно применены",Snackbar.LENGTH_SHORT)
        val v = snackBar.view
        v.setBackgroundColor(resources.getColor(R.color.colorGreen_900))
        snackBar.setTextColor(resources.getColor(R.color.white))

        snackBar.show()


    }

}
