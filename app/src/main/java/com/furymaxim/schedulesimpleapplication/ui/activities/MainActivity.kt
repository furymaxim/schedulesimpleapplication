package com.furymaxim.schedulesimpleapplication.ui.activities


import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.furymaxim.schedulesimpleapplication.R
import com.furymaxim.schedulesimpleapplication.ui.fragments.NewsFragment
import com.furymaxim.schedulesimpleapplication.ui.fragments.ScheduleFragment
import com.furymaxim.schedulesimpleapplication.ui.fragments.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
      //  window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.white));

      /*  val attrib = window.attributes
        attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES*/

        setDefaultItem()
    }



    private val navListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            var selectedFragment: Fragment? = null

            when (item.itemId) {
                R.id.nav_schedule -> {
                    selectedFragment = ScheduleFragment.newInstance()
                    bottomNavigation.menu.getItem(1).setIcon(R.drawable.a_ic_timetable_blue)
                    bottomNavigation.menu.getItem(1).isEnabled = false
                    bottomNavigation.menu.getItem(2).isEnabled = true
                    bottomNavigation.menu.getItem(0).isEnabled = true
                }
                R.id.nav_settings -> {
                    selectedFragment = SettingsFragment.newInstance()
                    bottomNavigation.menu.getItem(2).setIcon(R.drawable.a_ic_settings_blue)
                    bottomNavigation.menu.getItem(2).isEnabled = false
                    bottomNavigation.menu.getItem(1).isEnabled = true
                    bottomNavigation.menu.getItem(0).isEnabled = true
                }
                R.id.nav_news -> {
                    selectedFragment = NewsFragment.newInstance()
                    bottomNavigation.menu.getItem(0).setIcon(R.drawable.a_ic_newspaper_blue)
                    bottomNavigation.menu.getItem(0).isEnabled = false
                    bottomNavigation.menu.getItem(2).isEnabled = true
                    bottomNavigation.menu.getItem(1).isEnabled = true
                }

            }

            supportFragmentManager.beginTransaction().replace(R.id.fragment, selectedFragment!!).commit()

            true
        }

    private fun setDefaultItem() {

        bottomNavigation.setOnNavigationItemSelectedListener(navListener)
        bottomNavigation.selectedItemId = R.id.nav_schedule
        bottomNavigation.menu.getItem(1).setIcon(R.drawable.a_ic_timetable_blue)
    }


    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            super.onBackPressed()
            //additional code
        } else {
            supportFragmentManager.popBackStack()
        }
    }
}

