package com.furymaxim.schedulesimpleapplication.ui.fragments


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast

import com.furymaxim.schedulesimpleapplication.R
import com.furymaxim.schedulesimpleapplication.adapters.LessonAdapter
import com.furymaxim.schedulesimpleapplication.helpers.CalendarRowHelper
import com.furymaxim.schedulesimpleapplication.models.Lesson
import com.furymaxim.schedulesimpleapplication.utils.Consts
import com.furymaxim.schedulesimpleapplication.utils.timeline.EventView
import com.furymaxim.schedulesimpleapplication.utils.timeline.MyEvent
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_schedule.*
import java.util.*
import kotlin.collections.ArrayList

class ScheduleFragment : Fragment(), LessonAdapter.OnLessonClickListener, View.OnClickListener {

    private var sharedPreferences:SharedPreferences? = null
    private var groupNumber:String? = null

    private var isMondayAvailable:Boolean = true
    private var isTuesdayAvailable:Boolean = true
    private var isWednesdayAvailable:Boolean = true
    private var isThursdayAvailable:Boolean = true
    private var isFridayAvailable:Boolean = true
    private var isSaturdayAvailable:Boolean = true

    var currentDayName:String? =null

    private var lessonList = ArrayList<Lesson>()

    var layout: FrameLayout? = null

    var lessonAdapter = LessonAdapter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = context!!.getSharedPreferences(Consts.PREF, Context.MODE_PRIVATE)

        if(!sharedPreferences!!.contains("oldGroup")){
            layoutEnterGroupNumber.visibility = View.VISIBLE
            activity!!.bottomNavigation.menu.getItem(2).isVisible = false


            goToSettings.setOnClickListener {

                activity!!.bottomNavigation.menu.getItem(2).isEnabled = false
                activity!!.bottomNavigation.menu.getItem(1).isEnabled = true
                activity!!.bottomNavigation.menu.getItem(0).isEnabled = true

                activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, SettingsFragment.newInstance())
                    .commit()

            }
        }else{
            groupNumber = sharedPreferences!!.getString("oldGroup","")
            currentDayName = CalendarRowHelper.getCurrentDayName()

            setActiveDay(CalendarRowHelper.getCurrentDayName())
            setWeekDays()
            //setInactiveDays(CalendarRowHelper.getCurrentDayName())

            lessonList = loadSchedule(groupNumber,currentDayName)

            for(lesson in lessonList) {
                initTimeLineView(lesson)
            }
/*
            lessonAdapter.setItems(lessonList)*/
        }



    }


    private fun setActiveDay(currentDayName:String){

        dayMon.setOnClickListener(this)
        dayTue.setOnClickListener(this)
        dayWed.setOnClickListener(this)
        dayThu.setOnClickListener(this)
        dayFri.setOnClickListener(this)
        daySat.setOnClickListener(this)
        daySun.setOnClickListener(this)

        when(currentDayName){
            "пн" ->{
                numberMon.setBackgroundResource(R.drawable.selected_day_1)
                numberMon.setTextColor(resources.getColor(R.color.white))
                isMondayAvailable = false
                dayMon.setOnClickListener(null)
            }
            "вт" ->{
                numberTue.setBackgroundResource(R.drawable.selected_day_1)
                numberTue.setTextColor(resources.getColor(R.color.white))
                isTuesdayAvailable = false
                dayTue.setOnClickListener(null)
            }
            "ср" ->{
                numberWed.setBackgroundResource(R.drawable.selected_day_1)
                numberWed.setTextColor(resources.getColor(R.color.white))
                isWednesdayAvailable = false
                dayWed.setOnClickListener(null)
            }
            "чт" ->{
                numberThu.setBackgroundResource(R.drawable.selected_day_1)
                numberThu.setTextColor(resources.getColor(R.color.white))
                isThursdayAvailable = false
                dayThu.setOnClickListener(null)
            }
            "пт" ->{
                numberFri.setBackgroundResource(R.drawable.selected_day_1)
                numberFri.setTextColor(resources.getColor(R.color.white))
                isFridayAvailable = false
                dayFri.setOnClickListener(null)
            }
            "сб" ->{
                numberSat.setBackgroundResource(R.drawable.selected_day_1)
                numberSat.setTextColor(resources.getColor(R.color.white))
                isSaturdayAvailable = false
                daySat.setOnClickListener(null)
            }
            "вс" ->{
                numberSun.setBackgroundResource(R.drawable.selected_day_1)
                numberSun.setTextColor(resources.getColor(R.color.white))
                isWednesdayAvailable = false
                daySun.setOnClickListener(null)

            }
        }
    }

    private fun initTimeLineView(lesson: Lesson){
            when(getRandomNumber()){
                1 -> setLayoutForTimeLine(lesson,R.layout.schedule_item_layout_yellow)
                2 -> setLayoutForTimeLine(lesson,R.layout.schedule_item_layout_blue)
                3 -> setLayoutForTimeLine(lesson,R.layout.schedule_item_layout_green)
            }
    }

    private fun setLayoutForTimeLine(lesson: Lesson, layout: Int) {

            val startTime = getTime(lesson.startTime!!)
            val formattedStartTime = String.format(Locale.US,"%.1f",startTime)

            val endTime = getTime(lesson.endTime!!)
            val formattedEndTime = String.format(Locale.US,"%.1f",endTime)

            timeLine.addEvent(EventView(context,
                MyEvent(lesson.title!!,formattedStartTime.toFloat()-8f,formattedEndTime.toFloat()-8f),
                itemsMargin = 1, //optional
                layoutResourceId = layout,
                setupView = { myView ->
                    //SETUP VIEW
                    myView.findViewById<TextView>(R.id.item_title).text = lesson.title!!
                    myView.findViewById<TextView>(R.id.item_aud).text = lesson.aud!!
                    myView.findViewById<TextView>(R.id.item_teacher).text = lesson.teacher!!
                    myView.findViewById<TextView>(R.id.item_type).text = lesson.type!!
                },
                onItemClick = { event ->
                    //CLICK EVENT
                }
            ))
    }

    private fun getTime(timeWithNoZeroLeading: String): Float{
        val partA = timeWithNoZeroLeading.substring(0,timeWithNoZeroLeading.indexOf(":")).toFloat()

        if(timeWithNoZeroLeading[3] == '0' && timeWithNoZeroLeading[4] != '0'){
            val partB = timeWithNoZeroLeading.substring(timeWithNoZeroLeading.indexOf(":")+1,timeWithNoZeroLeading.length).toFloat()/60
            return partA + partB
        }else if(timeWithNoZeroLeading[3] != '0'){
            val partB = timeWithNoZeroLeading.substring(timeWithNoZeroLeading.indexOf(":")+1,timeWithNoZeroLeading.length).toFloat()/60
            return partA + partB
        }else{
            return partA
        }
    }

    private fun getRandomNumber():Int{
        return 1 + (Math.random() * ((3 - 1) + 1)).toInt()
    }


   private fun setInactiveDays(currentDayName:String) {

            when (currentDayName) {
                "пн" -> {
                    numberTue.setBackgroundResource(0)
                    nameTue.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberTue.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberWed.setBackgroundResource(0)
                    nameWed.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberWed.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberThu.setBackgroundResource(0)
                    nameThu.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberThu.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberFri.setBackgroundResource(0)
                    nameFri.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberFri.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSat.setBackgroundResource(0)
                    nameSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSat.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSun.setBackgroundResource(0)
                    nameSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                }
                "вт" -> {
                    numberMon.setBackgroundResource(0)
                    nameMon.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberMon.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberWed.setBackgroundResource(0)
                    nameWed.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberWed.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberThu.setBackgroundResource(0)
                    nameThu.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberThu.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberFri.setBackgroundResource(0)
                    nameFri.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberFri.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSat.setBackgroundResource(0)
                    nameSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSat.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSun.setBackgroundResource(0)
                    nameSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                }
                "ср" -> {
                    numberMon.setBackgroundResource(0)
                    nameMon.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberMon.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberTue.setBackgroundResource(0)
                    nameTue.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberTue.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberThu.setBackgroundResource(0)
                    nameThu.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberThu.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberFri.setBackgroundResource(0)
                    nameFri.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberFri.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSat.setBackgroundResource(0)
                    nameSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSat.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSun.setBackgroundResource(0)
                    nameSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                }
                "чт" -> {
                    numberMon.setBackgroundResource(0)
                    nameMon.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberMon.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberTue.setBackgroundResource(0)
                    nameTue.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberTue.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberWed.setBackgroundResource(0)
                    nameWed.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberWed.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberFri.setBackgroundResource(0)
                    nameFri.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberFri.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSat.setBackgroundResource(0)
                    nameSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSat.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSun.setBackgroundResource(0)
                    nameSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                }
                "пт" -> {
                    numberMon.setBackgroundResource(0)
                    nameMon.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberMon.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberTue.setBackgroundResource(0)
                    nameTue.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberTue.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberWed.setBackgroundResource(0)
                    nameWed.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberWed.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberThu.setBackgroundResource(0)
                    nameThu.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberThu.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSat.setBackgroundResource(0)
                    nameSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSat.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSun.setBackgroundResource(0)
                    nameSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                }
                "сб" -> {
                    numberMon.setBackgroundResource(0)
                    nameMon.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberMon.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberTue.setBackgroundResource(0)
                    nameTue.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberTue.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberWed.setBackgroundResource(0)
                    nameWed.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberWed.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberThu.setBackgroundResource(0)
                    nameThu.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberThu.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberFri.setBackgroundResource(0)
                    nameFri.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberFri.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSun.setBackgroundResource(0)
                    nameSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSun.setTextColor(resources.getColor(R.color.colorBlack_900))
                }
                "вс" -> {
                    numberMon.setBackgroundResource(0)
                    nameMon.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberMon.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberTue.setBackgroundResource(0)
                    nameTue.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberTue.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberWed.setBackgroundResource(0)
                    nameWed.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberWed.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberThu.setBackgroundResource(0)
                    nameThu.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberThu.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberFri.setBackgroundResource(0)
                    nameFri.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberFri.setTextColor(resources.getColor(R.color.colorBlack_900))

                    numberSat.setBackgroundResource(0)
                    nameSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                    numberSat.setTextColor(resources.getColor(R.color.colorBlack_900))
                }

        }
    }

    private fun setWeekDays(){
        val days = CalendarRowHelper.getDaysOfCurrentWeek()

        numberMon.text = (days[0].substring(0,2))
        numberTue.text = (days[1].substring(0,2))
        numberWed.text = (days[2].substring(0,2))
        numberThu.text = (days[3].substring(0,2))
        numberFri.text = (days[4].substring(0,2))
        numberSat.text = (days[5].substring(0,2))
        numberSun.text = (days[6].substring(0,2))
    }


    private fun loadSchedule(group:String?,currentDayName:String?):ArrayList<Lesson>{
        val lessonList = ArrayList<Lesson>()

        when(currentDayName){
            "пн" -> {
                lessonList.add(Lesson("11:50","13:20","Программирование на Java","Мун Д.O.","215","Упр."))
                lessonList.add(Lesson("13:50","15:10","Информационные технологии","Кирильчук М.В.","314","Лек."))
                lessonList.add(Lesson("15:30","17:00","ООП","Степнов Р.Д.","918","М"))
            }

            "вт" -> {
                lessonList.add(Lesson("11:50","13:20","Программирование на Java","Мун Д.O.","215","Упр."))
                lessonList.add(Lesson("13:50","15:10","Информационные технологии","Кирильчук М.В.","314","Лек."))
            }

            "ср" -> {
                lessonList.add(Lesson("08:30","10:00","Теория информации","Коротаев О.A.","613","Лаб."))
                lessonList.add(Lesson("10:10","11:40","Основы электроники","Князев В.С.","71","Лек."))
                lessonList.add(Lesson("11:50","13:20","Программирование на Java","Мун Д.O.","215","Упр."))
                lessonList.add(Lesson("13:50","15:10","Информационные технологии","Кирильчук М.В.","314","Лек."))
            }

            "чт" -> {
                lessonList.add(Lesson("11:50","13:20","Программирование на Java","Мун Д.O.","215","Упр."))
            }

            "пт" -> {
                lessonList.add(Lesson("13:50","15:10","Информационные технологии","Кирильчук М.В.","314","Лек."))
                lessonList.add(Lesson("15:30","17:00","ООП","Степнов Р.Д.","918","М"))
            }

            "сб" -> {
                lessonList.add(Lesson("08:30","10:00","Теория информации","Коротаев О.A.","613","Лаб."))
                lessonList.add(Lesson("10:10","11:40","Основы электроники","Князев В.С.","71","Лек."))
                lessonList.add(Lesson("11:50","13:20","Программирование на Java","Мун Д.O.","215","Упр."))
                lessonList.add(Lesson("13:50","15:10","Информационные технологии","Кирильчук М.В.","314","Лек."))
                lessonList.add(Lesson("15:30","17:00","ООП","Степнов Р.Д.","918","М"))
            }
            "вс" -> {
                lessonList.add(Lesson("10:30","11:40","Химия","Коротаев О.A.","613","Лаб."))
            }
        }

        return lessonList
    }


    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.dayMon -> {
                currentDayName = "пн"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                dayMon.setOnClickListener(null)
            }
            R.id.dayTue -> {
                currentDayName = "вт"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                dayTue.setOnClickListener(null)
            }
            R.id.dayWed -> {
                currentDayName = "ср"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                dayWed.setOnClickListener(null)
            }
            R.id.dayThu -> {
                currentDayName = "чт"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                dayThu.setOnClickListener(null)
            }

            R.id.dayFri -> {
                currentDayName = "пт"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                dayFri.setOnClickListener(null)
            }
            R.id.daySat -> {
                currentDayName = "сб"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                daySat.setOnClickListener(null)
            }
            R.id.daySun -> {
                currentDayName = "вс"
                setInactiveDays(currentDayName!!)
                setActiveDay(currentDayName!!)
                daySun.setOnClickListener(null)


            }

        }

        lessonList = loadSchedule(groupNumber,currentDayName)

        timeLine.clearAllEvents<MyEvent>()

        for(lesson in lessonList) {
            initTimeLineView(lesson)
        }

        /*lessonAdapter.clearItems()
        lessonAdapter.setItems(loadSchedule("",currentDayName))
        lessonAdapter.notifyDataSetChanged()*/
    }

    override fun onLessonClick(position: Int) {
        val currentPos = lessonAdapter.lessonList[position].title
        Toast.makeText(context,currentPos,Toast.LENGTH_SHORT).show()
    }


    companion object {

        fun newInstance(): ScheduleFragment {
            return ScheduleFragment()
        }
    }


}
