package com.furymaxim.schedulesimpleapplication.ui.fragments


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2

import com.furymaxim.schedulesimpleapplication.R
import com.furymaxim.schedulesimpleapplication.adapters.SliderAdapter
import com.furymaxim.schedulesimpleapplication.helpers.StringParser
import com.furymaxim.schedulesimpleapplication.utils.Consts
import com.furymaxim.schedulesimpleapplication.utils.Params

import kotlinx.android.synthetic.main.fragment_details.*
import me.relex.circleindicator.CircleIndicator
import me.relex.circleindicator.CircleIndicator3
import java.util.*
import kotlin.collections.ArrayList

class DetailsFragment : Fragment() {

    var imageLinks:String?=null
    var text:String?=null
    var date:String?=null
    private var sharedPreferences: SharedPreferences? =null

    private var imageModelArrayList: ArrayList<String?>? = null
    private var arrSize:Int ? = null
    private var indicator:CircleIndicator?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_details, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        getValuesFromBundle()

        sharedPreferences = context!!.getSharedPreferences(Consts.PREF, Context.MODE_PRIVATE)

        sharedPreferences!!.edit().putBoolean("fragmentSwitched",true).apply()

        mPager = view.findViewById(R.id.pager) as ViewPager
        indicator = view.findViewById(R.id.indicator) as CircleIndicator

        imageModelArrayList = ArrayList()

        if(imageLinks != null) {

            if(imageLinks!!.contains(",")) {
                val stringArray = StringParser.parseString(imageLinks)

                arrSize = stringArray.size
                imageModelArrayList = populateList()
            }else{
                imageModelArrayList!!.add(imageLinks)
            }
        }else {
            arrSize = 5

            imageModelArrayList = populateList()
        }

        init()

        newsDate.text = date
        newsText.text = text


        back.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(
                R.id.fragment,
                NewsFragment.newInstance()
            ).commit()
        }


    }

    private fun init() {


        mPager!!.adapter = SliderAdapter(context!!, this.imageModelArrayList!!)


        indicator!!.setViewPager(mPager)

        NUM_PAGES = imageModelArrayList!!.size

        // Auto start of viewpager //if want to stop just comment
        val handler = Handler()
        val update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            mPager!!.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        indicator!!.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })


    }



    private fun populateList(): ArrayList<String?> {

        val list = ArrayList<String?>()

        if(imageLinks!= null) {
            for (i in 0..arrSize!!.minus(1)) {
                list.add(StringParser.parseString(imageLinks)[i])
            }
        }
        else{
            for (i in 0..arrSize!!.minus(1)) {
                list.add(null)
            }

        }
        return list
    }


    companion object {

        fun newInstance(imageLinks: String?, text:String, date:String): DetailsFragment {

            val args = Bundle()

            args.putString(Params.LINKS,imageLinks)
            args.putString(Params.TEXT,text)
            args.putString(Params.DATE,date)

            val fragment = DetailsFragment()

            fragment.arguments = args

            return fragment
        }

        private var mPager: ViewPager? = null
        private var currentPage = 0
        private var NUM_PAGES = 0
    }

    private fun getValuesFromBundle(){
        val bundle = this.arguments

        if(bundle != null){
            imageLinks = bundle.getString(Params.LINKS)
            text = bundle.getString(Params.TEXT)
            date = bundle.getString(Params.DATE)
        }
    }


}
