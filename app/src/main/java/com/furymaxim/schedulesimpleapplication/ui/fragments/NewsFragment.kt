package com.furymaxim.schedulesimpleapplication.ui.fragments


import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.schedulesimpleapplication.R
import com.furymaxim.schedulesimpleapplication.adapters.PaginationAdapter
import com.furymaxim.schedulesimpleapplication.api.NetworkService
import com.furymaxim.schedulesimpleapplication.database.NewsDatabase
import com.furymaxim.schedulesimpleapplication.helpers.InternetConnectionChecker
import com.furymaxim.schedulesimpleapplication.helpers.InternetConnectionChecker.Companion.isOnline
import com.furymaxim.schedulesimpleapplication.helpers.OnNewsClickListener
import com.furymaxim.schedulesimpleapplication.helpers.PaginationListener
import com.furymaxim.schedulesimpleapplication.models.BasicModel
import com.furymaxim.schedulesimpleapplication.models.News
import com.furymaxim.schedulesimpleapplication.models.NewsFields
import com.furymaxim.schedulesimpleapplication.ui.activities.MainActivity
import com.furymaxim.schedulesimpleapplication.utils.Consts
import kotlinx.android.synthetic.main.fragment_news.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.ref.WeakReference

class NewsFragment : Fragment(), OnNewsClickListener{

    private var adapter:PaginationAdapter? = null
    private val PAGE_START = 1
    private var isLoading = false
    private var isLastPage = false
    private var totalPages = 0
    private val PER_PAGE = 20
    private var currentPage = PAGE_START
    private var linearLayoutManager: LinearLayoutManager? = null
    internal var newsDatabase: NewsDatabase? = null
    private var sharedPreferences: SharedPreferences? = null
    private var currentNewsId:Int? = 0
    private var startPos = 0
    private var endPos = 0
    private var availableNews = false
    private var results: MutableList<NewsFields>? = null
    private var storedNews: List<News>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newsDatabase = NewsDatabase.getInstance(context!!)

        sharedPreferences = context!!.getSharedPreferences(Consts.PREF,Context.MODE_PRIVATE)

        adapter = PaginationAdapter(activity, this)

        setupRecyclerView()

        recyclerView.addOnScrollListener(object: PaginationListener(linearLayoutManager) {
            override fun loadMoreItems(){
                isLoading = true
                currentPage += 1

                Handler().postDelayed( { loadNextPage() }, 1000)
            }

            override fun isLastPageT(): Boolean {
                return isLastPage
            }

            override fun isLoadingT(): Boolean {
                return isLoading
            }

            override fun getTotalPageCount(): Int {
                return totalPages
            }
        })

        if(!sharedPreferences!!.contains("fragmentSwitched")){
            sharedPreferences!!.edit().putBoolean("fragmentSwitched",false).apply()
        }


        if(sharedPreferences!!.getBoolean("fragmentSwitched",false)){

            fakelayout.visibility = View.GONE

            totalPages = 5
            startPos = 0
            endPos = getCount()+1
            currentNewsId = sharedPreferences!!.getInt("currentNewsId",0)
            currentPage = sharedPreferences!!.getInt("currentPage",0)

            if(currentPage == totalPages)isLastPage = true

            results = ArrayList()
            storedNews = getNewsWithinRange(startPos,endPos)

            for(news in storedNews!!){
                val newsFields = NewsFields()
                newsFields.title = news.title
                newsFields.releaseDate = news.date
                newsFields.overview = news.text
                if(isOnline()) {
                    newsFields.posterPath = news.image
                }else{
                    newsFields.posterPath = null
                }
                results!!.add(newsFields)
            }

            adapter!!.addAll(results)

            sharedPreferences!!.edit().putBoolean("fragmentSwitched",false).apply()
        }else {
                if (isOnline()) {
                    fakelayout.visibility = View.GONE
                    totalPages = 5
                    availableNews = true
                    Handler().postDelayed({ loadFirstPage() }, 1000)
                } else {
                    if (getCount() != 0) {
                        fakelayout.visibility = View.GONE
                        totalPages = getCount() / PER_PAGE
                        availableNews = true
                        Handler().postDelayed({
                            loadFirstPage()
                        }, 1000)
                    }
                }
        }
    }

    private fun loadFirstPage() {
        callTopRatedMoviesApi().enqueue(object : Callback<BasicModel?> {
            override fun onResponse(call: Call<BasicModel?>, response: Response<BasicModel?>) { // Got data. Send it to adapter
                results = fetchResults(response) as MutableList<NewsFields>

                if(progressBar!=null) progressBar.visibility = View.GONE

                DeleteAllNewsAsyncTask(newsDatabase).execute()

                for(news in results!!) {
                    currentNewsId= currentNewsId!!.plus(1)
                    insertNews(news)
                }

                adapter!!.addAll(results)

                if (currentPage < totalPages) adapter!!.addLoadingFooter() else isLastPage = true

                call.cancel()
            }

            override fun onFailure(call: Call<BasicModel?>, t: Throwable) {
                call.cancel()
                startPos = 0
                endPos = 21

                results = ArrayList()
                storedNews = getNewsWithinRange(startPos, endPos)

                for (news in storedNews!!) {
                    val newsFields = NewsFields()
                    newsFields.title = news.title
                    newsFields.releaseDate = news.date
                    newsFields.overview = news.text
                    newsFields.posterPath = null

                    results!!.add(newsFields)
                    }

                if(progressBar!=null) {
                    progressBar.visibility = View.GONE
                }

                adapter!!.addAll(results)

                if (currentPage < totalPages) adapter!!.addLoadingFooter() else isLastPage = true
                }
        })
    }

    private fun loadNextPage() {
        callTopRatedMoviesApi().enqueue(object : Callback<BasicModel?> {
            override fun onResponse(call: Call<BasicModel?>, response: Response<BasicModel?>) {

                adapter!!.removeLoadingFooter()
                isLoading = false

                results = fetchResults(response) as MutableList<NewsFields>

                for (news in results!!) {
                    currentNewsId = currentNewsId!!.plus(1)
                    insertNews(news)
                }

                adapter!!.addAll(results)


                if (currentPage != totalPages) adapter!!.addLoadingFooter() else isLastPage = true
            }

            override fun onFailure(call: Call<BasicModel?>, t: Throwable) {
                call.cancel()

                adapter!!.removeLoadingFooter()
                isLoading = false

                startPos = endPos-1
                endPos = endPos.plus(20)

                results = ArrayList()
                storedNews = getNewsWithinRange(startPos,endPos)

                for(news in storedNews!!){
                    val newsFields = NewsFields()
                    newsFields.title = news.title
                    newsFields.releaseDate = news.date
                    newsFields.overview = news.text
                    newsFields.posterPath = null

                    results!!.add(newsFields)
                }

                adapter!!.addAll(results)

                sharedPreferences!!.edit().putInt("newsAvailable",endPos-1).apply()

                if (currentPage != totalPages) adapter!!.addLoadingFooter() else isLastPage = true

            }
        })
    }


  private fun insertNews(newsFields: NewsFields){
      val news = News()

      news.title = newsFields.title
      news.date = newsFields.releaseDate
      news.text = newsFields.overview
      news.image = newsFields.posterPath
      news.id = currentNewsId
      InsertNewsAsyncTask(newsDatabase).execute(news)
    }


    private fun getCount():Int{

        return GetCountAsyncTask(newsDatabase).execute().get()!!
    }


    private fun getNewsWithinRange(startId:Int, endId:Int):List<News>{

        return GetNewsWithingRangeAsyncTask(newsDatabase).execute(startId,endId).get()!!
    }


    override fun onNewsClick(position: Int) {
        val currentPos = adapter!!.newsFields[position]

        sharedPreferences!!.edit().putInt("currentNewsId",currentNewsId!!).apply()
        sharedPreferences!!.edit().putInt("currentPage",currentPage).apply()


        activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragment, DetailsFragment.newInstance(currentPos.posterPath,currentPos.overview, currentPos.releaseDate))
            .addToBackStack(null)
            .commit()
    }


    private fun callTopRatedMoviesApi():Call<BasicModel>{
       return NetworkService.getInstance()
            .getJSONApi()
            .getTopRatedMovies(Consts.API_KEY,"en_US",currentPage)!!

    }

    private fun fetchResults(response: Response<BasicModel?>): List<NewsFields> {
        val topRatedMovies: BasicModel? = response.body()
        return topRatedMovies!!.results
    }

    private fun setupRecyclerView(){
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        recyclerView.layoutManager = linearLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = adapter
    }


    private  class GetNewsWithingRangeAsyncTask

    internal constructor(val newsDatabase: NewsDatabase?): AsyncTask<Int, Void?, List<News>?>() {


        override fun doInBackground(vararg params: Int?): List<News>? {

            return newsDatabase!!.newsDAO().getNewsByIdRange(params[0]!!,params[1]!!)
        }
    }



    private class DeleteAllNewsAsyncTask
        internal constructor(val newsDatabase: NewsDatabase?): AsyncTask<Void?, Void?, Void?>() {

        override fun doInBackground(vararg params: Void?): Void? {
            newsDatabase!!.newsDAO().deleteAllNews()

            return null
        }
    }


    private  class InsertNewsAsyncTask
    internal constructor(val newsDatabase: NewsDatabase?): AsyncTask<News?, Void?, Void?>() {

        override fun doInBackground(vararg params: News?): Void? {
            newsDatabase!!.newsDAO().insertNews(params[0]!!)

            return null
        }
    }


    private  class GetCountAsyncTask
    internal constructor(val newsDatabase: NewsDatabase?): AsyncTask<Void?, Void?, Int?>() {

        override fun doInBackground(vararg params: Void?): Int? {
            return newsDatabase!!.newsDAO().getCount()
        }

    }


    companion object {

        fun newInstance(): NewsFragment {
            return NewsFragment()
        }
    }

    override fun  onResume() {
        super.onResume()

        if(recyclerView !=null) {
            recyclerView.scrollToPosition(sharedPreferences!!.getInt("position", 0))
            Handler().postDelayed({
                recyclerView.scrollBy(0, -sharedPreferences!!.getInt("offset", 0)
                )
            }, 250)
        }
    }

    override fun onPause() {
        super.onPause()

        if (fakelayout.visibility != View.VISIBLE) {
            val firstChild = recyclerView.getChildAt(0)
            val firstVisiblePosition = recyclerView.getChildAdapterPosition(firstChild)

            if(firstChild !=null) {
                val offset = firstChild.top

                sharedPreferences!!.edit()
                    .putInt("position", firstVisiblePosition)
                    .putInt("offset", offset)
                    .apply()
            }
        }
    }





}






