package com.furymaxim.schedulesimpleapplication.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.furymaxim.schedulesimpleapplication.R;
import com.furymaxim.schedulesimpleapplication.models.Lesson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.LessonViewHolder> {

    private List<Lesson> lessonList = new ArrayList<>();
    private OnLessonClickListener mOnLessonClickListener;

    public List<Lesson> getLessonList() {
        return lessonList;
    }

    public LessonAdapter(OnLessonClickListener onLessonClickListener) {
        this.mOnLessonClickListener = onLessonClickListener;
    }

    public class LessonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView startTime, endTime, title, teacher, type, aud;
        OnLessonClickListener onLessonClickListener;

        public LessonViewHolder(View itemView, OnLessonClickListener onLessonClickListener) {
            super(itemView);

/*            startTime = itemView.findViewById(R.id.startLesson);
            endTime = itemView.findViewById(R.id.endLesson);*/
            title = itemView.findViewById(R.id.item_title);
            teacher = itemView.findViewById(R.id.item_teacher);
            type = itemView.findViewById(R.id.item_type);
            aud = itemView.findViewById(R.id.item_aud);

            this.onLessonClickListener = onLessonClickListener;
        }

        @Override
        public void onClick(View v) {
            onLessonClickListener.onLessonClick(getAdapterPosition());
        }

        public void bind(Lesson lesson) {
            startTime.setText(lesson.getStartTime());
            endTime.setText(lesson.getEndTime());
            title.setText(lesson.getTitle());
            teacher.setText(lesson.getTeacher());
            type.setText(lesson.getType());
            aud.setText(lesson.getAud());
        }
    }


    public void setItems(Collection<Lesson> lessons) {
        lessonList.addAll(lessons);
        notifyDataSetChanged();
    }

    public void clearItems() {
        lessonList.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_item_layout_yellow, parent, false);

        return new LessonViewHolder(view, mOnLessonClickListener);

    }

    @Override
    public void onBindViewHolder(@NonNull LessonViewHolder holder, int position) {
        holder.bind(lessonList.get(position));
    }

    @Override
    public int getItemCount() {
        return lessonList.size();
    }

    public interface OnLessonClickListener{
        void onLessonClick(int position);
    }

}
