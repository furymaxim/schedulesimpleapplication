package com.furymaxim.schedulesimpleapplication.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.furymaxim.schedulesimpleapplication.R;
import com.furymaxim.schedulesimpleapplication.helpers.OnNewsClickListener;
import com.furymaxim.schedulesimpleapplication.models.ItemNews;
import com.furymaxim.schedulesimpleapplication.models.NewsFields;
import com.furymaxim.schedulesimpleapplication.utils.Consts;


import java.util.ArrayList;
import java.util.List;


public class PaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<NewsFields> newsFields;
    private Context context;
    public OnNewsClickListener mOnNewsClickListener;

    private boolean isLoadingAdded = false;



    public PaginationAdapter(Context context, OnNewsClickListener onNewsClickListener) {
        this.context = context;
        this.mOnNewsClickListener = onNewsClickListener;

        newsFields = new ArrayList<>();
    }

    public List<NewsFields> getNewsFields() {
        return newsFields;
    }

    public void setNewsFields(List<NewsFields> newsList) {
        newsFields = newsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_loading, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.item_news, parent, false);
        viewHolder = new NewsVH(v1,mOnNewsClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        NewsFields newsField = newsFields.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                NewsVH viewHolder = (NewsVH) holder;
                SpannableString spannableString = null;
                if(newsField.getOverview().length() > 120){
                    spannableString = new SpannableString(holder.itemView.getContext().getString(R.string.expand, newsField.getOverview().substring(0,120)));
                }
                viewHolder.titleTV.setText(newsField.getTitle());
                if(newsField.getOverview().length() > 120) {
                    viewHolder.textTV.setText(spannableString);
                }else{
                    viewHolder.textTV.setText(newsField.getOverview());
                }
                viewHolder.dateTV.setText(newsField.getReleaseDate().substring(0, 4));
                if(newsField.getPosterPath() == null){
                    viewHolder.image.setImageResource(R.drawable.img_placeholder);
                }else {
                    Glide
                            .with(context)
                            .load(Consts.BASE_URL_IMG + newsField.getPosterPath())
                            .listener(new RequestListener<Drawable>() {

                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    viewHolder.mProgress.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    viewHolder.mProgress.setVisibility(View.GONE);
                                    return false;
                                }

                            })
                            .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                            .centerCrop()
                            .into(viewHolder.image);
                }

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return newsFields == null ? 0 : newsFields.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == newsFields.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(NewsFields newsField) {
        newsFields.add(newsField);
        notifyItemInserted(newsFields.size() - 1);
    }

    public void addAll(List<NewsFields> newFieldsList) {
        for (NewsFields newsField : newFieldsList) {
            add(newsField);
        }
    }

    public void remove(NewsFields newsField) {
        int position = newsFields.indexOf(newsField);
        if (position > -1) {
            newsFields.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new NewsFields());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = newsFields.size() - 1;
        NewsFields item = getItem(position);

        if (item != null) {
            newsFields.remove(position);
            notifyItemRemoved(position);
        }
    }

    public NewsFields getItem(int position) {
        return newsFields.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class NewsVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView dateTV, titleTV, textTV;
        private ImageView image;
        private ProgressBar mProgress;
        OnNewsClickListener onNewsClickListener;

        public NewsVH(View itemView, OnNewsClickListener onNewsClickListener) {
            super(itemView);

            titleTV = itemView.findViewById(R.id.textTitle);
            dateTV = itemView.findViewById(R.id.textDate);
            textTV = itemView.findViewById(R.id.textNews);
            image = itemView.findViewById(R.id.myImageView);
            this.onNewsClickListener = onNewsClickListener;

            mProgress = itemView.findViewById(R.id.newsProgress);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onNewsClickListener.onNewsClick(getAdapterPosition());
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }





}