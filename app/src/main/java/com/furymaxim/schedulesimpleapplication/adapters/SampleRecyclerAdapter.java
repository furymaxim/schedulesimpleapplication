package com.furymaxim.schedulesimpleapplication.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.furymaxim.schedulesimpleapplication.R;

import java.util.ArrayList;
import java.util.List;

public class SampleRecyclerAdapter extends RecyclerView.Adapter<SampleRecyclerAdapter.TextViewHolder> {

    protected int mCount;
    protected List<String> imageLinks;
    protected Context context;

    public SampleRecyclerAdapter(Context context, ArrayList<String> imageLinks) {
        mCount = imageLinks.size();
        this.context = context;
        this.imageLinks = imageLinks;
    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return TextViewHolder.createViewHolder(parent);
    }

    @Override public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        holder.bindView(position);
    }

    @Override public int getItemCount() {
        return mCount;
    }

    public void add() {
        int position = mCount;
        mCount++;
        notifyItemInserted(position);
    }

    public void remove() {
        if (mCount == 0) {
            return;
        }
        mCount--;
        int position = mCount;
        notifyItemRemoved(position);
    }

    static class TextViewHolder extends RecyclerView.ViewHolder {



        static TextViewHolder createViewHolder(@NonNull ViewGroup parent) {



            return new TextViewHolder(parent);
        }

        TextViewHolder(@NonNull View itemView) {
            super(itemView);
            ImageView image = itemView.findViewById(R.id.image);
            ProgressBar mProgress= itemView.findViewById(R.id.itemProgress);
        }


        void bindView(int position) {

        }
    }
}
