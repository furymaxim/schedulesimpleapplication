package com.furymaxim.schedulesimpleapplication.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.furymaxim.schedulesimpleapplication.R
import com.furymaxim.schedulesimpleapplication.utils.Consts
import kotlin.collections.ArrayList

class SliderAdapter(private val context:Context, private val imageLinkList: ArrayList<String?>?): PagerAdapter(){


    private val inflater: LayoutInflater = LayoutInflater.from(context)



    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return imageLinkList!!.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slider_item, container, false)!!

        val imageView = imageLayout.findViewById(R.id.image) as ImageView
        val mProgress = imageLayout.findViewById(R.id.itemProgress) as ProgressBar


        if(imageLinkList!![position] !=null) {
            Glide
                .with(context)
                .load(Consts.BASE_URL_IMG + imageLinkList[position])
                .listener(object : RequestListener<Drawable> {

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {
                        mProgress.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        mProgress.visibility = View.GONE
                        return false
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                .centerCrop()
                .into(imageView)


        }else{
            imageView.setImageResource(R.drawable.img_placeholder)
        }

        container.addView(imageLayout, 0)

        return imageLayout
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {

    }

    override fun saveState(): Parcelable? {
        return null
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


}