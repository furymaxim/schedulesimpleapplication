package com.furymaxim.schedulesimpleapplication.utils

class Params {

    companion object{
        const val LINKS = "IMAGES"
        const val TEXT = "TEXT"
        const val DATE = "DATE"
    }
}