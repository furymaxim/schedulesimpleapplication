package com.furymaxim.schedulesimpleapplication.utils.timeline

open class Event{

    open var title: String = ""
    open var startTime = 0f
    open var endTime = 0f

}