package com.furymaxim.schedulesimpleapplication.utils

class Consts {

    companion object{
        const val BASE_URL = "https://api.themoviedb.org/3/movie/"
        const val API_KEY = "b279b19a802d7f7a557620d4878d39ae"
        const val BASE_URL_IMG = "https://image.tmdb.org/t/p/w500"
        const val PREF = "MY_PREF"
    }
}