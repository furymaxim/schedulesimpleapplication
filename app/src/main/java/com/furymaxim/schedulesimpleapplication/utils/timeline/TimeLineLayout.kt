package com.furymaxim.schedulesimpleapplication.utils.timeline

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import android.widget.ScrollView

class TimeLineLayout : ScrollView {
    private lateinit var scrollView: ScrollView

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    {
        scrollView = ScrollView(context)
        scrollView.addView(TimeLineLayoutGroup(context,attrs))
        addView(scrollView)
        post {
            scrollView.requestLayout()
        }
    }

    fun <T : Event>addEvent(child: EventView<T>?) {
        (scrollView.getChildAt(0) as ViewGroup).addView(child)
    }

    fun <T : Event>clearAllEvents() {
        (scrollView.getChildAt(0) as ViewGroup).removeAllViews()
    }
}