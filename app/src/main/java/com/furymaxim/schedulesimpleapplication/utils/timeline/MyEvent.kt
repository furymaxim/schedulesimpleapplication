package com.furymaxim.schedulesimpleapplication.utils.timeline

class MyEvent: Event {
    override var title: String
    override var startTime: Float
    override var endTime: Float

    constructor(title: String, startTime: Float, endTime: Float){
        this.title = title
        this.endTime = endTime
        this.startTime = startTime
    }
}