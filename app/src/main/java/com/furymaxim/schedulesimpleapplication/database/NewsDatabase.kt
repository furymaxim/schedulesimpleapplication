package com.furymaxim.schedulesimpleapplication.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.furymaxim.schedulesimpleapplication.database.NewsDatabase.Companion.DATABASE_VERSION
import com.furymaxim.schedulesimpleapplication.local.NewsDAO
import com.furymaxim.schedulesimpleapplication.models.News

@Database(entities = arrayOf(News::class),version = DATABASE_VERSION ,exportSchema = false)
abstract class NewsDatabase:RoomDatabase(){
    abstract fun newsDAO(): NewsDAO


    companion object{
        const val DATABASE_VERSION = 3
        const val DATABASE_NAME = "SimpleScheduleDB"

        private var mInstance: NewsDatabase?= null

        fun getInstance(context: Context): NewsDatabase {
            if(mInstance == null)
                mInstance = Room.databaseBuilder(context,
                    NewsDatabase::class.java,
                    DATABASE_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()

            return mInstance!!
        }
    }

}