package com.furymaxim.schedulesimpleapplication.models


class ItemNews{

    var title:String? = null
    var text:String? = null
    var date:String? = null
    var imageLink:String? = null
    var fullText:String? = null

    constructor(title: String, text: String, date: String, imageLink: String, fullText: String){
        this.title = title
        this.text = text
        this.date = date
        this.imageLink = imageLink
        this.fullText = fullText

    }
    constructor()
}