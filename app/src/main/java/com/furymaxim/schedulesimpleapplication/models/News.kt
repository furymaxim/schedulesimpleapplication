package com.furymaxim.schedulesimpleapplication.models

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news")
class News {

    @NonNull
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:Int? = 0


    @ColumnInfo(name = "title")
    var title:String? = null

    @ColumnInfo(name = "text")
    var text:String? = null

    @ColumnInfo(name = "date")
    var date:String? = null

    @ColumnInfo(name = "image")
    var image:String?=null



}