package com.furymaxim.schedulesimpleapplication.helpers

interface OnNewsClickListener {
    fun onNewsClick(position:Int)
}