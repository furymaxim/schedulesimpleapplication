package com.furymaxim.schedulesimpleapplication.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class CalendarRowHelper {


        public static String getCurrentDayName(){
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            return new SimpleDateFormat("E", new Locale("ru")).format(date.getTime());

        }

        public static String getCurrentDay(){

            return new SimpleDateFormat("dd/MM/yyyy", new Locale("ru")).format(Calendar.getInstance().getTime());
        }

        public static String[] getDaysOfCurrentWeek(){
            Calendar now = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM",Locale.ENGLISH);

            String[] days = new String[7];

            if(getCurrentDayName().equals("Вс")) {
                int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) - 5;
                now.add(Calendar.DAY_OF_MONTH, delta);
                for (int i = 0; i < 7; i++) {
                    days[i] = format.format(now.getTime());
                    now.add(Calendar.DAY_OF_MONTH, 1);
                }
            }else{
                int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2;
                now.add(Calendar.DAY_OF_MONTH, delta);
                for (int i = 0; i < 7; i++) {
                    days[i] = format.format(now.getTime());
                    now.add(Calendar.DAY_OF_MONTH, 1);
                }
            }

            return days;
        }
}

