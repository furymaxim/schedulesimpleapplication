package com.furymaxim.schedulesimpleapplication.helpers

import android.os.AsyncTask
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class InternetConnectionChecker{



    companion object{

        fun isOnline():Boolean {

            val runtime = Runtime.getRuntime()
            try {
                val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
                val exitValue = ipProcess.waitFor()
                return (exitValue == 0)
            }
            catch (e:IOException)          { e.printStackTrace(); }
            catch (e:InterruptedException) { e.printStackTrace(); }

            return false

        }
    }
}